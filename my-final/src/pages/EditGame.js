import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios'
import {UserContext} from "../context/UserContext"
import {useHistory, useParams} from "react-router-dom"
//import Material UI
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import { Input, TextareaAutosize } from '@material-ui/core';


const useStyles = makeStyles({
    form:{
        width:"60%",
        margin:"0 auto",
    },
    left: {
      float:"left",
    },
    right:{
        float:"right",
        width:"300px"
    },
  });

const CreateMovie =()=>{
    let {id} = useParams();
    const [user] = useContext(UserContext)
    const [dataGame, setdataGame] = useState(null);
    const [data] =useState(null)
    const [input,setInput] = useState({
        id:null, 
        name: "" , 
        genre: "",
        platform: "",
        release: "",
        singlePlayer:"",
        multiplayer:"",
        image_url:""})
    const classes = useStyles();
    let history = useHistory();

    
        useEffect(() => {
            if (dataGame === null){
              axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
              .then(res => {
                setdataGame(res.data)
              })
            }
          }, [dataGame]);

    
        useEffect(() => {
            if (input.id === null){
              axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
              .then(res => {
                setInput(res.data)
              })
            }
          }, [input, setInput]);
    

    
    

          const handleChange = (event) =>{
            let value = event.target.value
            let name = event.target.name
            switch (name){
            case "name":{
                setInput({...input, name: value})
                break;
            }
            case "genre":{
                setInput({...input, genre: value})
                break;
            }
            case "platform":{
                setInput({...input, platform: value})
                break;
            }
            case "release":{
                setInput({...input, release: value})
                break;
            }
            case "single":{
                setInput({...input, singlePlayer: value})
                break;
            }
            case "multi":{
                setInput({...input, multiplayer: value})
                break;
            }
            case "image":{
                setInput({...input, image_url: value})
                break;
            }
            default:{break;}
            }
    
        }

    const handleSubmit = (event) =>{
        event.preventDefault();
        if ( input.id !== null){
            console.log(input.id)
            axios.put(`https://backendexample.sanbersy.com/api/data-game/${input.id}`,
            {  name: input.name , 
                genre: input.genre,
                platform: input.platform,
                release: input.release,
                singlePlayer:input.singlePlayer,
                multiplayer:input.multiplayer,
                image_url:input.image_url}, {headers: {"Authorization" : `Bearer ${user.token}`
            }}).then(res => {
                console.log(dataGame)
                let data = res.data;
                let newDataGame = dataGame.map(item => {
                if (item.id === input.id){
                    item.name = data.name
                    item.genre = data.genre
                    item.platform = data.platform
                    item.release = data.release
                    item.singlePlayer = data.singlePlayer
                    item.multiplayer = data.multiplayer
                    item.image_url=data.image_url
                }
                return item
                })
                setdataGame(newDataGame)
                setInput({ id:null, 
                    name: "" , 
                    genre: "",
                    platform: "",
                    release: "",
                    singlePlayer:"",
                    multiplayer:"",
                    image_url:""})
                    alert("Data Game berhasil di Update")
                    history.push("/GameEditor")
            })
        }else{
           alert("Data tidak berhasil diperbarui")

        }
    }

    return(
        <>
        <div class="wrap2">
            <h4>Edit Game</h4>
            
            <form onSubmit={handleSubmit} className={classes.form}>
                <br />
                <label  className={classes.left}>Name </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Name" 
                variant="outlined" type="text" name="name" onChange={handleChange} value={input.name} />
                <br />
                <br />
                <br />
                <label  className={classes.left}>Genre </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Genre" 
                variant="outlined" type="text" name="genre" onChange={handleChange} value={input.genre} />
                <br />
                <br />
                <br />
                <label  className={classes.left}> Platform </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Platform" 
                variant="outlined" type="text" name="platform" onChange={handleChange} value={input.platform} />
                <br />
                <br />
                <br />
                <label  className={classes.left}> Release </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Release" 
                variant="outlined" type="number" min="1990" name="release" onChange={handleChange} value={input.release} />
                 <br />
                <br />
                <br />
                <label  className={classes.left}> Single Player </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Release" 
                variant="outlined" type="number" max="1" name="single" onChange={handleChange} value={input.singlePlayer} />
                <br />
                <br />
                <br />
                <label  className={classes.left}> Multi Player </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Release" 
                variant="outlined" type="number" max="1" name="multi" onChange={handleChange} value={input.multiplayer} />
                <br />
                <br />
                <br />
                <label  className={classes.left}>Image URL </label>
                <TextField required className={classes.right} id="outlined-multiline-static"
                label="Image URL"
                multiline
                rows={5}
                name="image" value={input.image_url} onChange={handleChange}
                variant="outlined"/>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <center>
                <button>
                <Button>
                    Submit
                </Button>
                </button>
                </center>
                <br />
                <br />
                <br />
                <br />
            </form>
        </div>
        </>
    )
}

export default CreateMovie