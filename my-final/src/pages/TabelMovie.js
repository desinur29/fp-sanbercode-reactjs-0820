import React, { useContext, useEffect, useState } from 'react';
import {useHistory} from "react-router-dom"
import axios from 'axios'
import {UserContext} from "../context/UserContext"
//Material UI/ANTD
import { Table, Space, Tooltip } from 'antd';
import 'antd/dist/antd.css';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  table: {
    width:"80%",
    margin:"0 auto"
  },
  search:{
    width:"500px",
    margin:"0 auto"
  }
});


const TabelMovie = () =>{

    const [user ] = useContext(UserContext)
    const [search, setSearch] = useState("");
    const [text,setText] = useState("")
    const [dataMovie,setdataMovie] = useState(null);
    const [input,setInput] = useState({
      id:null, 
      title: "" , 
      description : "",
      year : " ",
      duration: "",
      genre:"",
      rating:"",
      image_url:""})

      let history = useHistory();
      const classes = useStyles();

      useEffect(() => {
        if (dataMovie === null){
          axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
          .then(res => {
              console.log(res.data)
            setdataMovie(res.data)
          })
        }
      }, [dataMovie]);

      const deleteData = (event) =>{
        console.log("kerja dia")
        
        let idMovie = parseInt(event.target.value)
        alert(idMovie)
        if (idMovie!==null){
          axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`, {headers: {"Authorization" : `Bearer ${user.token}`}})
          .then(res => {
          let newdataMovie = dataMovie.filter(el=> el.id !== idMovie)
          setdataMovie(newdataMovie)
          setInput({ id:null, 
           title: "" , 
           description : "",
           year : " ",
           duration: "",
           genre:"",
           rating:"",
           image_url:""})
           alert("data berhasil dihapus")
           
         })
        }else{
          alert("tidak berhasil dihapus")
        }
    }

    const editData = (event) =>{
        let idMovie = parseInt(event.target.value)
        history.push(`/MovieEditor/edit/${idMovie}`)
    
        }
        const submitSearch = (event) =>{
          event.preventDefault()
          axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
          .then(res => {
            if(search !== ""){
                let pencarian = dataMovie.filter(el=>el.title.toLowerCase() === search.toLowerCase() ||el.description.toLowerCase() === search.toLowerCase()||el.year === parseInt(search)||el.duration === parseInt(search)||el.genre.toLowerCase() === search.toLowerCase()||el.rating === parseInt(search))
                setdataMovie(pencarian)
                setSearch("")
            }else{
                setdataMovie(res.data);
            }
                
          })
      
          }
        
      
        const handleChangeSearch = (event)=>{
          setSearch(event.target.value)
        }

        const handlecreate =()=>{
          history.push("/MovieEditor/create")
      }

    const columns = [
        {
          title: 'Title',
          dataIndex: 'title',
          sorter: (a, b) => a.title.length - b.title.length,
          sortDirections: ['descend', 'ascend'],
         
        },
        {
          title: 'Description',
          dataIndex: 'description',
          defaultSortOrder: 'descend',
          ellipsis: {
            showTitle: false,
          },
          render: description => (
            <Tooltip placement="topLeft" title={description}>
              {description}
            </Tooltip>
          ),
          sorter: (a, b) => a.description.length - b.description.length,
          sortDirections: ['descend', 'ascend'],
         
        },
        {
          title: 'Year',
          dataIndex: 'year',
          // filters: [
          //   {
          //     text: 'London',
          //     value: 'London',
          //   },
          //   {
          //     text: 'New York',
          //     value: 'New York',
          //   },
          // ],
          // filterMultiple: false,
          //onFilter: (value, record) => record.address.indexOf(value) === 0,
          sorter: (a, b) => a.year - b.year,
          sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            // filters: [
            //   {
            //     text: 'London',
            //     value: 'London',
            //   },
            //   {
            //     text: 'New York',
            //     value: 'New York',
            //   },
            // ],
            // filterMultiple: false,
            // //onFilter: (value, record) => record.address.indexOf(value) === 0,
            sorter: (a, b) => a.duration - b.duration,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Genre',
            dataIndex: 'genre',
            // filters: [
            //   {
            //     text: 'London',
            //     value: 'London',
            //   },
            //   {
            //     text: 'New York',
            //     value: 'New York',
            //   },
            // ],
            // filterMultiple: false,
            //onFilter: (value, record) => record.address.indexOf(value) === 0,
            sorter: (a, b) => a.genre.length - b.genre.length,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Rating',
            dataIndex: 'rating',
            // filters: [
            //   {
            //     text: '1',
            //     value: '',
            //   },
            //   {
            //     text: '2',
            //     value:'',
            //   },
            //   {
            //     text: '9',
            //     value: '9',
            //   },
            // ],
            // filterMultiple: false,
            //onFilter: (value, record) => record.rating.indexOf(value) === 0,
            sorter: (a, b) => a.rating - b.rating,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Action',
            dataIndex:'id',
            key: 'id',
            render: (text, record) => (
              <Space size="left">
                <button value={record.id} onClick={deleteData} style={{marginRight:"5px"}}>Delete</button>
                <button value={record.id} onClick={editData}>Edit</button>
              </Space>
            ),
          },
      ];
      
      
      function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
      }

      return(
          <>
          <div class="wrap2">
          <h4>Table Movie</h4>
            <br />
            <div className={classes.search}> 
            <form onSubmit={submitSearch}>
              <center>
              <input style={{marginRight:"5px"}}type="text" value={search} onChange={handleChangeSearch} /> 
              <button>search</button>
              </center>
             
            </form>
          </div>
          <br />
          <br />
          <Table columns={columns} dataSource={dataMovie} onChange={onChange} />
          </div>
          </>
       
      )

}


export default TabelMovie