import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios'
import {UserContext} from "../context/UserContext"
import { Modal, Button } from 'antd';


function Movie (){
    const [user] = useContext(UserContext)
    const [dataMovie, setdataMovie] = useState(null);

    useEffect(() => {
        if (dataMovie === null){
          axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
          .then(res => {
            setdataMovie(res.data)
          })
        }
      }, [dataMovie]);

      const info = (event) =>{
          const idMovie = parseInt(event.target.value)
          let movie = dataMovie.find(el=>el.id === idMovie)
          Modal.info({
            title: 'Info Movie',
            width:"500px",
           content: (
              <div>
                <strong>Title : {movie.title}</strong><br />
                <strong>Rating : {movie.rating}</strong><br />
                <strong>Duration : {movie.duration}</strong><br/>
                <strong>Genre : {movie.genre}</strong><br />
                <strong>Year : {movie.year}</strong><br />

                <p>Description :</p>
                <p>{movie.description}</p>
              </div>
            ),
            onOk() {},
          });
          
      }
    


    return(
        <>
            <div className={user? 'wrap2' : 'wrap'}>
            <h1 style={{textAlign:"center"}}>Daftar Film Terbaik</h1>
                {dataMovie !== null && dataMovie.map(el =>
                    <div className="list">
                        <h2 style={{textAlign:"center"}}>{el.title}</h2>
                        <div className="bio">
                            <div className="image">
                            <img src={el.image_url} /> 
                            </div>
                            <div className="info">
                               
                                <button style={{margin:"50px", width:"150px", height:"40px", backgroundColor:"#6666ff", color:"white",border:"none",borderRadius: "4px"}} value={el.id} onClick={info}>Info</button>
                                
                            </div>
                        </div>
                        <hr />
                    </div>
                    )}
            </div>
  

        
        
        </>
    )
}

export default Movie