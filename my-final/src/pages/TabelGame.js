import React, { useContext, useEffect, useState } from 'react';
import {useHistory} from "react-router-dom"
import axios from 'axios'
import {UserContext} from "../context/UserContext"
//Material UI/ANTD
import { Table, Space, Tooltip } from 'antd';
import 'antd/dist/antd.css';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  table: {
    width:"80%",
    margin:"0 auto"
  },
  search:{
    width:"500px",
    margin:"0 auto"
  }
});


const TabelGame = () =>{

    const [dataGame, setdataGame] = useState(null);
    const [search, setSearch] = useState("")
    const [user] = useContext(UserContext)
    const classes = useStyles();
    let history = useHistory();
    const [input,setInput] = useState({
      id:null, 
      name: "" , 
      genre: "",
      platform: "",
      release: "",
      image_url:""})



    const deleteData = (event) =>{
      console.log("kerja dia")
      let idGame = parseInt(event.target.value)
      if (idGame!==null){
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {headers: {"Authorization" : `Bearer ${user.token}`}})
        .then(res => {
        let newdataGame = dataGame.filter(el=> el.id !== idGame)
        setdataGame(newdataGame)
        setInput({ id:null, 
          name: "" , 
          genre: "",
          platform: "",
          release: "",
          image_url:""})
         alert("data berhasil dihapus")
       })
      }else{
        alert("tidak berhasil dihapus")
      }
    } 

    function editData(event) {
      let idGame = parseInt(event.target.value);
      history.push(`/GameEditor/edit/${idGame}`);
    }

    const submitSearch = (event) =>{
      event.preventDefault()
      axios.get(`https://backendexample.sanbersy.com/api/data-game`)
      .then(res => {
        if(search !== ""){
            let pencarian = dataGame.filter(el=>el.name.toLowerCase() === search.toLowerCase() ||el.genre.toLowerCase() === search.toLowerCase()||el.platform.toLowerCase() === search.toLowerCase() ||parseInt(el.release) === parseInt(search))
            setdataGame(pencarian)
            setSearch("")
        }else{
            setdataGame(res.data);
        }
            
      })
  
      }
    
  
    const handleChangeSearch = (event)=>{
      setSearch(event.target.value)
    }

    useEffect(() => {
        if (dataGame === null){
            axios.get(`https://backendexample.sanbersy.com/api/data-game`)
          .then(res => {
            setdataGame(res.data)
          })
        }
      }, [dataGame]);

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          sorter: (a, b) => a.name.length - b.name.length,
          sortDirections: ['descend', 'ascend'],
         
        },
        {
          title: 'Genre',
          dataIndex: 'genre',
          defaultSortOrder: 'descend',
          ellipsis: {
            showTitle: false,
          },
          render: description => (
            <Tooltip placement="topLeft" title={description}>
              {description}
            </Tooltip>
          ),
          sorter: (a, b) => a.genre.length- b.genre.length,
          sortDirections: ['descend', 'ascend'],
         
        },
        {
          title: 'Platform',
          dataIndex: 'platform',
          // filters: [
          //   {
          //     text: 'London',
          //     value: 'London',
          //   },
          //   {
          //     text: 'New York',
          //     value: 'New York',
          //   },
          // ],
          // filterMultiple: false,
          // onFilter: (value, record) => record.address.indexOf(value) === 0,
          sorter: (a, b) => a.platform.length - b.platform.length,
          sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Release',
            dataIndex: 'release',
            // filters: [
            //   {
            //     text: 'London',
            //     value: 'London',
            //   },
            //   {
            //     text: 'New York',
            //     value: 'New York',
            //   },
            // ],
            // filterMultiple: false,
            // onFilter: (value, record) => record.address.indexOf(value) === 0,
            sorter: (a, b) => a.release - b.release,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Single Player',
            dataIndex: 'singlePlayer',
            // filters: [
            //   {
            //     text: 'London',
            //     value: 'London',
            //   },
            //   {
            //     text: 'New York',
            //     value: 'New York',
            //   },
            // ],
            // filterMultiple: false,
            // onFilter: (value, record) => record.address.indexOf(value) === 0,
            sorter: (a, b) => a.singlePlayer - b.singlePlayer,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Multi Player',
            dataIndex: 'multiplayer',
            // filters: [
            //   {
            //     text: 'London',
            //     value: 'London',
            //   },
            //   {
            //     text: 'New York',
            //     value: 'New York',
            //   },
            // ],
            // filterMultiple: false,
            // onFilter: (value, record) => record.address.indexOf(value) === 0,
            sorter: (a, b) => a.multiplayer - b.multiplayer,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Action',
            dataIndex:'id',
            key: 'id',
            render: (text, record) => (
              <Space size="left">
                <button value={record.id} onClick={deleteData} style={{marginRight:"5px"}}>Delete</button>
                <button value={record.id} onClick={editData}>Edit</button>
              </Space>
            ),
          },
      ];
      
      
      function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
      }

      return(
          <>
          <div class="wrap2">
          <h4>Table Game</h4>
            <br />
            <div className={classes.search}> 
            <form onSubmit={submitSearch}>
              <center>
              <input style={{marginRight:"5px"}}type="text" value={search} onChange={handleChangeSearch} /> 
              <button>search</button>
              </center>
             
            </form>
          </div>
          <br />
          <br />
          <Table columns={columns} dataSource={dataGame} onChange={onChange} />
          </div>
          </>
       
      )

}


export default TabelGame