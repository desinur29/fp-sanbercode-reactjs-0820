import React, { useContext, useState } from "react"
import {UserContext} from "../context/UserContext"
import axios from "axios"
import {useHistory, useParams} from "react-router-dom"
//import Material UI
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const Register = () =>{

    const [ ,setUser] = useContext(UserContext)
    const [input, setInput] = useState({name: "", email: "" , password: ""})
    let history = useHistory();

    
    const handleChange = (event)=>{
        let value = event.target.value;
        let name = event.target.name;

        switch (name) {
            case "name":{
                setInput({...input, name: value})
                break;
              }
              case "email":{
                setInput({...input, email: value})
                break;
              }
              case "password":{
                setInput({...input, password: value})
                break;
              }
              default:{break;}
            }
        
        }
        

    const handleSubmit = (event) =>{
       
        event.preventDefault();
        console.log(input.name,input.email,input.password);
        axios.post("Https://backendexample.sanbersy.com/api/register",{
            name :input.name,
            email : input.email,
            password : input.password
        }).then(
            (res)=>{
                console.log(res)
                let user = res.data.user
                let token = res.data.token
                console.log(token)
                let currentUser = {name: user.name, email: user.email,password:user.password, token }
                setUser(currentUser)
                localStorage.setItem("user", JSON.stringify(currentUser))
                console.log("berhasil kirim data")
                alert("berhasil Sign Up")
                history.push("/login");

            }
        ).catch((err)=>{
            alert(err)
          })
        
    }
    
    return(
        <div class="logreg">
            <h1>Create your account </h1>
            <form onSubmit={handleSubmit}>
                <br />
                <TextField required id="outlined-basic" label="Username" variant="outlined" type="text" name="name" onChange={handleChange} value={input.name} />
                <br />
                <br />
                <TextField required id="outlined-basic" label="Email" variant="outlined" type="email" name="email" onChange={handleChange} value={input.email}/>
                <br />
                <br />
                <TextField required id="outlined-basic" label="Password"  type="password" variant="outlined" name="password" minlength="6" onChange={handleChange} value={input.password}  />
                <br />
                <br />
                <button>
                <Button>
                Sign Up
                </Button>

                </button>
                
                <br />
                <br />
               
                
            </form>
        

        </div>
    )
}

export default Register