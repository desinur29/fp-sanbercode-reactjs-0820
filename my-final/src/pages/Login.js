import React, {useContext, useState} from 'react';
import {UserContext} from "../context/UserContext"
import {useHistory} from "react-router-dom"
import axios from "axios"
//import Material UI
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const Login = () =>{
    let history = useHistory()
    const [, setUser] = useContext(UserContext)
    const [input, setInput] = useState({email: "" , password: ""})


    const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "email":{
        setInput({...input, email: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }


    }
    const handleSubmit = (event)=>{
        event.preventDefault()
        axios.post("https://backendexample.sanbersy.com/api/user-login", {
        email: input.email, 
        password: input.password
        }).then(
        (res)=>{
            var user = res.data.user
            var token = res.data.token
            console.log(token)
            var currentUser = {name: user.name, email: user.email, token }
            setUser(currentUser)
            localStorage.setItem("user", JSON.stringify(currentUser))
            alert('Berhasil Log in')
            history.push("/")
           
        }
        ).catch((err)=>{
        alert(err)
        })

    }

    

    return(
      
        <div class="wrap">
             <div class="logreg">
            <h1>Log in to Website</h1>
            <form onSubmit={handleSubmit} >
                <br />
                <TextField required id="outlined-basic" label="Email" variant="outlined" name="email" onChange={handleChange} value={input.email} />
                <br />
                <br />
                <TextField required id="outlined-basic" label="Password" type="password" variant="outlined" name="password" onChange={handleChange} value={input.password}/>
                <br />
                <br />
                <button>
                <Button>
                Login
                </Button>
                </button>
                <br />
                <br />
            </form>
        </div>
        </div>
       
        
            
        

    )
}

export default Login