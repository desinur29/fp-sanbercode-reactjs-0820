import React, { useContext, useState } from 'react';
import axios from 'axios'
import {UserContext} from "../context/UserContext"
import {useHistory} from "react-router-dom"
//import Material UI
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import { Input, TextareaAutosize } from '@material-ui/core';


const useStyles = makeStyles({
    form:{
        width:"60%",
        margin:"0 auto",
    },
    left: {
      float:"left",
    },
    right:{
        float:"right",
        width:"300px"
    },
  });

const CreateMovie =()=>{
    
    const [user] = useContext(UserContext)
    const [dataMovie, setdataMovie] = useState(null);
    const [input,setInput] = useState({
        id:null, 
        title: "" , 
        description : "",
        year : "2020",
        duration: "120",
        genre:"",
        rating:"0",
        image_url:""})
    const classes = useStyles();
    let history = useHistory();

    const handleChange = (event) =>{
        let value = event.target.value
        let name = event.target.name
        switch (name){
        case "title":{
            setInput({...input, title: value})
            break;
        }
        case "description":{
            setInput({...input, description: value})
            break;
        }
        case "year":{
                setInput({...input, year: value})
                break;
        }
        case "duration":{
            setInput({...input, duration: value})
            break;
        }
        case "genre":{
            setInput({...input, genre: value})
            break;
        }
        case "rating":{
            setInput({...input, rating: value})
            break;
        }
        case "image":{
            setInput({...input, image_url: value})
            break;
        }
        default:{break;}
        }

    }

    const handleSubmit = (event) =>{
        event.preventDefault();
        if ( input.id === null){
            axios.post(`https://backendexample.sanbersy.com/api/data-movie`, 
            {   title: input.title , 
                description : input.description,
                year : input.year,
                duration: input.duration,
                genre: input.genre,
                rating: input.rating,
                image_url:input.image_url}, {headers: {"Authorization" : `Bearer ${user.token}`}})
            .then(res => {
              console.log(res)
              var data = res.data
              setdataMovie([dataMovie, {
                  id: data.id, 
                  name: data.name, 
                  description:data.description,
                  year:data.year,
                  duration:data.duration,
                  genre : data.genre,
                  rating : data.rating,
                  image_url : data.image_url
                }])
              setInput({ id:null, 
                title: "" , 
                description : "",
                year : "2020",
                duration: "120",
                genre:"",
                rating:"0",
                image_url:""})
                alert("Data berhasil di Input")
                history.push("/MovieEditor")
            }).catch(
              (err)=>{console.log(err)}
            )
      
        }else{
            console.log("data tidak berhasil ditambahkan !")
        }
    }

    return(
        <>
        <div class="wrap2">
            <h4>Add Movie</h4>
            
            <form onSubmit={handleSubmit} className={classes.form}>
                <br />
                <label  className={classes.left}>Title </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Title" 
                variant="outlined" type="text" name="title" onChange={handleChange} value={input.title} />
                <br />
                <br />
                <br />
                <label  className={classes.left}>Description </label>
                <TextField required className={classes.right} id="outlined-multiline-static"
                label="Description"
                multiline
                rows={3}
                name="description" value={input.description} onChange={handleChange}
                variant="outlined"/>
                <br />
                <br />
                <br />
                <br />
                <br />
                <label  className={classes.left}>Year </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Year" 
                variant="outlined" type="number"  inputProps={{ min: 1980 }}name="year" min="1980" onChange={handleChange} value={input.year} />
                <br />
                <br />
                <br />
                <label  className={classes.left}>Duration </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Duration" 
                variant="outlined" type="number" inputProps={{ min: 30 }}name="duration" min="30" onChange={handleChange} value={input.duration} />
                <br />
                <br />
                <br />
                <label  className={classes.left}>Genre </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Genre" 
                variant="outlined" type="text" name="genre" onChange={handleChange} value={input.genre} />
                <br />
                <br />
                <br />
                <label  className={classes.left}> Rating </label>
                <TextField required className={classes.right}  id="outlined-basic" label="Rating" 
                variant="outlined" type="number" inputProps={{ min: 0, max: 10 }} name="rating" onChange={handleChange} value={input.rating} />
                 <br />
                <br />
                <br />
                <label  className={classes.left}>Image URL </label>
                <TextField required className={classes.right} id="outlined-multiline-static"
                label="Image URL"
                multiline
                rows={5}
                name="image" value={input.image_url} onChange={handleChange}
                variant="outlined"/>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <center>
                <button>
                <Button>
                    Submit
                </Button>
                </button>
                </center>
                <br />
                <br />
                <br />
                <br />
            </form>
        </div>
        </>
    )
}

export default CreateMovie