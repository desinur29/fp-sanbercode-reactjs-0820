import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios'
import {UserContext} from "../context/UserContext"
import { Modal } from 'antd';



const Game = () =>{
    const [user] = useContext(UserContext)
    const [dataGame, setdataGame] = useState(null);

    useEffect(() => {
        if (dataGame === null){
          axios.get(`https://backendexample.sanbersy.com/api/data-game`)
          .then(res => {
            setdataGame(res.data)
          })
        }
      }, [dataGame]);

      const info = (event) =>{
        const idGame = parseInt(event.target.value)
        let game = dataGame.find(el=>el.id === idGame)
        Modal.info({
          title: 'Info Game',
          width:"500px",
         content: (
            <div>
                <strong>Genre : {game.genre}</strong><br />
                <strong>Platform : {game.platform}</strong><br />
                <strong>Release : {game.release}</strong><br />
                <strong>Single Player : {game.singlePlayer}</strong><br />
                <strong>Multi Player : {game.multiplayer}</strong><br />
            </div>
          ),
          onOk() {},
        });
        
    }
    


    return(
        <>
            <div className={user? 'wrap2' : 'wrap'}>
        <h1 style={{textAlign:"center"}}>Daftar Game Terbaik</h1>
            {dataGame !== null && dataGame.map(el =>
                <div class="list">
                    <h2 style={{textAlign:"center"}}>{el.name}</h2>
                    <div className="bio">
                            <div className="image">
                            <img src={el.image_url} /> 
                            </div>
                            <div className="info">
                                <button style={{margin:"50px", width:"150px", height:"40px", backgroundColor:"#6666ff", color:"white",border:"none",borderRadius: "4px"}}value={el.id} onClick={info}>Info</button>
                            </div>
                        </div>
                    <hr />
                </div>
                )}
        </div>
        </>
    )
}

export default Game