import React, { useContext, useState } from 'react';
import axios from 'axios'
import {UserContext} from "../context/UserContext"
import {useHistory} from "react-router-dom"
//import Material UI
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import userEvent from '@testing-library/user-event';

const ChangePass = () =>{
    const [user] = useContext(UserContext)
    const [input, setInput] = useState({current_password :"", new_password:"" , new_confirm_password:""})
    const [password,setPassword] =useState("")
    let history = useHistory();

    const handleChange = (event) =>{
        let value = event.target.value
        let name = event.target.name
        switch (name){
            case "password":{
                setInput({...input, current_password: value})
                break;
            }
        case "password1":{
            setInput({...input, new_password: value})
            break;
        }
        case "password2":{
            setInput({...input, new_confirm_password: value})
            break;
        }
        default:{break;}
        }
    }
    
    const handleSubmit = (event) =>{
        event.preventDefault();
        if(input.new_password === input.new_confirm_password){
            axios.post("https://backendexample.sanbersy.com/api/change-password", 
            {   current_password: input.current_password , 
                new_password: input.new_password,
                new_confirm_password: input.new_confirm_password,
            }, {headers: {"Authorization" : `Bearer ${user.token}`}})
            .then(res => {
              console.log(res)
              var data = res.data
              setPassword([password, {
                current_password: data.current_password , 
                new_password: data.new_password,
                new_confirm_password: data.new_confirm_password,
                }])
              setInput({ current_password :"", new_password:"" , new_confirm_password:""})
                alert("Password berhasil di Ganti")
                history.push("/")
            }).catch(
              (err)=>{console.log(err)}
            )
        }else{
            alert("new password and confirmation not same")
            setInput({ current_password :"", new_password:"" , new_confirm_password:""})
        }
    }

    return(
        <div class="wrap2">
            <div class="logreg">
            <h1>Change Password </h1>
            <form onSubmit={handleSubmit}>
                <br />
                <TextField required id="outlined-basic" label="Current Password" variant="outlined" type="password" name="password" onChange={handleChange} value={input.current_password}/>
                <br />
                <br />
                <TextField required id="outlined-basic" label="New Password" variant="outlined" type="password" name="password1" onChange={handleChange} value={input.new_password}/>
                <br />
                <br />
                <TextField required id="outlined-basic" label="Confirm Password"  type="password" variant="outlined" name="password2" minlength="6" onChange={handleChange} value={input.new_confirm_password}  />
                <br />
                <br />
                <button>
                <Button>
                Change Password
                </Button>

                </button>
                
                <br />
                <br />
               
                
            </form>

            </div>
             
        </div>
    )
}

export default ChangePass