import React from 'react';
//import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

export default function Sidebar() {

    const styleAppbar = {
        height:"40px",
      }
    
      
     
    
      const StylingNavbar ={
        color:"blue"
      }
 return(
     
     <div id="sidebar">
         <div style={{marginTop:"60px"}}>
         <Link style={StylingNavbar} to="/MovieEditor/create" color="inherit">Add Movie</Link>
         <hr />
         <Link style={StylingNavbar} to="/GameEditor/create" color="inherit">Add Game</Link>
         <hr />
         </div>
         
     </div>
 )

}
