import React, {useContext} from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
// Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import {UserContext} from "../context/UserContext"

const useStyles = makeStyles((theme) => ({
  root: {
   
    background:  "#6666ff",
  },
  menuButton: {
    marginRight: theme.spacing(1),
  },
  title: {
    flexGrow: 1,
  },
}));



export default function Header() {

  const [user, setUser] = useContext(UserContext)
  const classes = useStyles();
  const styleAppbar = {
    height:"40px",
  }

  const styleButton={
    top:-12,
  }
 

  const StylingNavbar ={
    color:"white"
  }
  

  const handleLogout =()=>{
    setUser(null)
    localStorage.removeItem("user")
    alert('Berhasil Log out')
  }

  const handleName =() =>{

  }
  
  
  return (
    <div>
      <AppBar position="fixed"  className={classes.root} style={styleAppbar}>
        <Toolbar>
          <Grid container  justify="space-between">
            <Grid item xs>
              
              
              <Button style={styleButton}> <Link style={StylingNavbar} to="/" color="inherit">Movie</Link></Button>
             
             
                <Button style={styleButton}> <Link style={StylingNavbar} to="/Game" color="inherit">Game</Link></Button>
                
              {user !== null && ( 
                <>
                
                <Button style={styleButton}> <Link style={StylingNavbar} to="/MovieEditor" color="inherit">Movie Editor</Link></Button>
                
                
                <Button style={styleButton}><Link style={StylingNavbar} to="/GameEditor" color="inherit">Game Editor</Link></Button>
                </>
                )}
                <Grid item xs style={{float:"right"}}>
                <div>
                  {
                    user === null && (
                      <>
                       
                        <Button style={styleButton} > <Link style={StylingNavbar} to="/login" color="inherit"> SIGN IN </Link></Button>
                       
                       
                        <Button style={styleButton}> <Link style={StylingNavbar} to="/register" color="inherit">SIGN UP</Link></Button>
                        
                      </>
                    )
                  }

                  {
                    user !== null && (
                      <>
                        <Button color="inherit" style={styleButton} onClick={handleName}>
                          {user.name}
                        </Button>
          
                        <Button  style={styleButton} className={classes.button}>
                        <Link style={StylingNavbar} to="/ChangePass" >Change Password </Link>
                        </Button>
                        
                        <Button color="inherit" style={styleButton} onClick={handleLogout} >
                          Logout
                        </Button>
                      </>
                    )
                  }
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}
