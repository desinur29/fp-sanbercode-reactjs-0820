import React, {useContext} from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import {UserContext} from "../context/UserContext"
// { Layout, Menu, Breadcrumb } from 'antd';
//import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';

import Header from "./Header";
import Section from "./Section"
import Footer from "./Footer"

//const { SubMenu } = Menu;
//const { Head, Content, Side,Foot } = Layout;


const Main = () => {
    const [user, setUser] = useContext(UserContext)
    
    return(
        <>
        <Router>
            
                <Header />
               
                <Section />
                
                <Footer />
            
        </Router>
        </>
    )
        
    

}

export default Main