import React, {useContext} from "react"
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Movie from "../pages/Movie"
import Game from "../pages/Game"
import Login from "../pages/Login";
import Register from "../pages/Register"
import Sidebar from "./Sidebar"
import ChangePass from "../pages/Changepass"
import CreateMovie from "../pages/CreateMovie"
import EditMovie from "../pages/EditMovie"
import CreateGame from "../pages/CreateGame"
import EditGame from "../pages/EditGame"
import {UserContext} from "../context/UserContext"
import TabelMovie from "../pages/TabelMovie"
import TabelGame from "../pages/TabelGame"

const Section = () =>{

  const[user] = useContext(UserContext);

  const PrivateRoute = ({user, ...props }) => {
    if (user) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const RegisterRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;

  const LoginRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;
  


    return(
   
    <section >
     
      <Switch>
        <Route exact path="/" user={user} component={Movie} />
        <Route exact path="/Game" user={user} component={Game} />
        <LoginRoute exact path="/login" user={user} component={Login}/>
        <RegisterRoute exact path="/register"  user={user} component={Register}/>
        <PrivateRoute exact path="/MovieEditor" user={user} component={TabelMovie} />
        <PrivateRoute exact path="/GameEditor" user={user} component={TabelGame} />
        <PrivateRoute exact path="/ChangePass" user={user} component={ChangePass} />
        <PrivateRoute exact path="/MovieEditor/create" user={user} component={CreateMovie} />
        <PrivateRoute exact path="/MovieEditor/edit/:id" user={user} component={EditMovie} />
        <PrivateRoute exact path="/GameEditor/create" user={user} component={CreateGame} />
        <PrivateRoute exact path="/GameEditor/edit/:id" user={user} component={EditGame} />
      </Switch>
      {user !== null && (<Sidebar></Sidebar>)}
    </section>

    )
}

export default Section